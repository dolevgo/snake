import pygame
from snake import Snake, Square
from random import choice

pygame.init()

GRAY = (128,128,128)
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)

def draw_grid(win, win_length, line_count, color = GRAY):
    gap = win_length // line_count
    for i in range(line_count):
        pygame.draw.line(win, color, (0, i * gap), (win_length, i * gap))
        pygame.draw.line(win, color, (i * gap, 0), (i * gap, win_length))

def draw(win, win_length, line_count, snake, food):
    win.fill(WHITE)
    food.draw(win)
    snake.draw(win)
    draw_grid(win, win_length, line_count, GRAY)
    pygame.display.update()

def handle_input(key, snake):
    dir=()
    if key == pygame.K_UP:
        dir = (0, -1)
    elif key == pygame.K_DOWN:
        dir = (0, 1)
    elif key == pygame.K_LEFT:
        dir = (-1, 0)
    elif key == pygame.K_RIGHT:
        dir = (1, 0)

    snake.change_direction(dir)

def generate_food_location(snake, line_count):
    snake_positions = [(s.x, s.y) for s in snake.body]
    snake_positions.append((snake.head.x, snake.head.y))

    available = [(x, y) for x in range(line_count) for y in range(line_count) if (x, y) not in snake_positions]

    return choice(available)

def generate_food(snake, line_count, color, dimensions):
    pos = generate_food_location(snake, line_count)
    return Square(pos, dimensions, color)
def main():
    pygame.display.set_caption("Snake!")
    WIN_LENGTH = 800
    LINE_COUNT = 40
    GRID_GAP = WIN_LENGTH // LINE_COUNT

    DIED = 0
    ATE = 1

    WIN = pygame.display.set_mode((WIN_LENGTH, WIN_LENGTH))
    FPS = 16
    clock = pygame.time.Clock()
    running = True

    snake = Snake(BLACK, GRID_GAP, (LINE_COUNT // 2, LINE_COUNT // 2))
    food = generate_food(snake, LINE_COUNT, RED, GRID_GAP)
    while running:
        clock.tick(FPS)
        draw(WIN, WIN_LENGTH, LINE_COUNT, snake, food)
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                elif event.key in [pygame.K_UP, pygame.K_DOWN, pygame.K_LEFT, pygame.K_RIGHT]:
                    handle_input(event.key, snake)

        status = snake.update(food, LINE_COUNT)
        if status[DIED]:
            snake = Snake(BLACK, GRID_GAP, (LINE_COUNT // 2, LINE_COUNT // 2))
        elif status[ATE]:
            food = generate_food(snake, LINE_COUNT, RED, GRID_GAP)

    pygame.quit()

if __name__ == '__main__':
    main()
