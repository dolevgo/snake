import pygame

class Square:
    def __init__(self, pos, length, color):
        self.x, self.y = pos
        self.length = length
        self.color = color

    def draw(self, win):
        pygame.draw.rect(win, self.color, (self.x * self.length, self.y * self.length, self.length, self.length))

    def clone(self):
        return Square((self.x, self.y), self.length, self.color)

    def meeting(self, other):
        return self.x == other.x and self.y == other.y

class Snake:
    def __init__(self, color, dimension, startPos):
        self.color = color
        self.dimension = dimension
        self.head = Square(startPos, self.dimension, self.color)
        self.body = []
        self.vx = 1
        self.vy = 0

    def change_direction(self, dir):
        vx, vy = dir
        if self.vx != -vx and self.vy != -vy:
            self.vx, self.vy = dir

    def update(self, food, bounds):
        ateFood = self.head.meeting(food)
        died = False

        if self.body:
            if not ateFood:
                self.body.pop() # Remove tail end
            self.body.insert(0, self.head)
            self.head = self.head.clone()
        else:
            if ateFood:
                self.body.insert(0, self.head)
                self.head = self.head.clone()
        self.head.x = (self.head.x + self.vx)%bounds
        self.head.y = (self.head.y + self.vy)%bounds

        positions = [(square.x, square.y) for square in self.body]
        died = (self.head.x, self.head.y) in positions
        return (died, ateFood)

    def draw(self, win):
        for square in self.body:
            square.draw(win)
        self.head.draw(win)
